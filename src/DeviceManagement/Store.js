import {applyMiddleware, combineReducers, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { composeWithDevTools } from 'redux-devtools-extension';
import {backendsReducer, deviceReducer} from "./Reducers";

const loggerMiddleware = createLogger();
const rootReducer = combineReducers({backends: backendsReducer, devices: deviceReducer});

const middleware = store => next => action => {console.log(action.type); next(action)};

export const store = createStore(rootReducer, composeWithDevTools(__DEV__?applyMiddleware(
    thunk,
    promise,
    loggerMiddleware,
    middleware
    ):applyMiddleware(
    thunk,
    promise,
    )
    )
);
