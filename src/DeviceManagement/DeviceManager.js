import {createAction} from "redux-starter-kit";

export const addConnector = createAction('ADD_CONNECTOR');
export const scanStopped = createAction('STOP_SCAN');
export const scanStarted = createAction('START_SCAN');
export const deviceDiscovered = createAction('DEVICE_DISCOVERED');
export const requestDeviceState = createAction('REQUEST_DEVICE_STATE');
export const deviceStateUpdate = createAction('DEVICE_STATE_UPDATE');
export const stripStateUpdate = createAction('STRIP_STATE_UPDATE');
export const stripEffectArgumentsUpdate = createAction('STRIP_EFFECT_ARGUMENTS_UPDATE');
export const setDefaultEffectArguments = createAction('SET_DEFAULT_EFFECT_ARGUMENTS');
export const startedDeviceRequest = createAction('STARTED_DEVICE_REQUEST');



export function startScan() {
    return (dispatch, getState) => {
        getState().backends.backends.forEach(
            (backend) => backend.startScan(
                (device) => dispatch(deviceDiscovered(device))));
        dispatch(scanStarted());
    }
}

export function stopScan() {
    return (dispatch, getState) => {
        getState().backends.backends.forEach((backend) => backend.stopScan());
        dispatch(scanStopped());
    }
}

export function toggleLights(deviceId, state){
    return(dispatch, getState) => {
        const device = getState().devices[deviceId];
        return dispatch({
            type: 'TOGGLE_LIGHTS',
            payload:{
                promise: device.backend.toggleLights(deviceId, state)
            },
            meta: {deviceId: deviceId}
        })
    }
}

export function setEffectArgs(deviceId, stripId, effectArgs) {
    return (dispatch, getState) => {
        const device = getState().devices[deviceId];
            return dispatch({
                type: 'SET_EFFECT_ARGUMENTS',
                payload: {
                    promise: device.backend.setEffectArguments(deviceId, stripId, effectArgs)
                },
                meta: {
                    deviceId: deviceId,
                    stripId: stripId,
                    oldArgs: device.config.stripConfig[stripId].effectArgs,
                    newArgs: effectArgs
                }
            }).catch((reason) => {
        });
    }
}

export function getDeviceConfig(deviceId) {
    return (dispatch, getState) => {
        const device = getState().devices[deviceId];
        return dispatch(connectToDevice(device)).then(() => {
            return dispatch({
                type: 'GET_DEVICE_CONFIG',
                payload: {
                    promise: device.backend.getDeviceConfig(deviceId)
                },
                meta: {deviceId: deviceId}
            })
        });
    }
}

export function getCompleteDeviceConfig(deviceId) {
    return (dispatch, getState) => {
        return dispatch(getDeviceConfig(deviceId)).then(() => {
            const device = getState().devices[deviceId];
            return Promise.all(Object.keys(device.config.stripConfig).map((stripId) => {
                dispatch(getEffectArguments(deviceId, stripId));
            }))
        }).then( () => {
            const device = getState().devices[deviceId];
            device.backend.observeSystemStatus(deviceId, (systemStatus) => {
                dispatch({type: 'SYSTEM_STATUS_UPDATE', payload: systemStatus, meta: {deviceId}});
            });
        })
    }
}

export function getEffectArguments(deviceId, stripId) {
    return (dispatch, getState) => {
        const device = getState().devices[deviceId];
        dispatch(connectToDevice((device))).then(() => {
            dispatch({
                type: 'GET_EFFECT_ARGS',
                payload: {
                    promise: device.backend.getEffectArguments(deviceId, stripId)
                },
                meta: {deviceId: device.id, stripId: stripId}
            });
        });
    }
}

export function changeEffect(deviceId, stripId, effect){
    return (dispatch, getState) => {
        const device = getState().devices[deviceId];
        const oldArgs = device.config.stripConfig[stripId].effectArgs;
        const effectArgName =  effect.charAt(0).toLowerCase() + effect.slice(1) + "Arguments";
        const newArgs = {...oldArgs,[effectArgName]: device.backend.getDefaultEffectArgs(deviceId, `${effect}Arguments`), [oldArgs.effectArguments]: undefined, effectArguments: effectArgName};
        return dispatch(setEffectArgs(deviceId, stripId, newArgs));
    }
}

export function setDeviceConfig(deviceId, config){
    return(dispatch, getState) => {
        const device = getState().devices[deviceId];
        return dispatch({
            type: "SET_CONFIG",
            payload:{
                promise: device.backend.setDeviceConfig(deviceId, {...config, stripConfig: Object.values(config.stripConfig), inputs: {gpioInputs: Object.values(config.inputs.gpioInputs)}})
            },
            meta: {deviceId: deviceId}
        }).catch(()=>{})
    }
}


export function connectToDevice(device) {
    if(device.pendingActions === 0) {
        return {
            type: 'CONNECT_TO_DEVICE',
            payload: {
                promise: device.connected ?
                    Promise.resolve() :
                    device.backend.connectToDevice(device.id).catch(() => {
                    })
            },
            meta: {deviceId: device.id}
        };
    }else{
        return {type: "dummy", payload: { promise: Promise.resolve()}};
    }
}

export function disconnectDevice(deviceId){
    return(dispatch, getState) => {
        const device = getState().devices[deviceId];
        return dispatch({
            type: "DISCONNECT_DEVICE",
            payload:{
                promise: device.backend.disconnectDevice(deviceId)
            },
            meta: {deviceId: deviceId}
        }).catch(()=>{})
    }
}
