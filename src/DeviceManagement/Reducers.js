import {createReducer} from "redux-starter-kit";
import {addConnector, deviceDiscovered, deviceStateUpdate, scanStarted, scanStopped} from "./DeviceManager";

function effectArgTypeToName(argType){
    return (argType.charAt(0).toUpperCase() + argType.slice(1)).replace("Arguments", "");
}

export const backendsReducer = createReducer({scanning: false, backends: new Array(0)}, {
    [addConnector]: (state, action) => {
        state.backends.push(action.payload);
    },
    [scanStarted]: (state, action) => {
        state.scanning = true;
    },
    [scanStopped]: (state, action) => {
        state.backends.forEach((backend) => backend.stopScan());
        state.scanning = false;
    }
});
export const deviceReducer = createReducer({}, {
    [deviceDiscovered]: (state, action) => {
        action.payload.pendingActions = 0;
        state[action.payload.id] = action.payload;
    },
    [deviceStateUpdate]: (state, action) => {
        const device = state[action.payload.id];
        device.state = action.payload.state;
    },
    ['CONNECT_TO_DEVICE_FULFILLED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = true;
        device.failed = false;
    },
    ['CONNECT_TO_DEVICE_REJECTED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = false;
        device.failed = true;
    },
    ['GET_DEVICE_CONFIG_FULFILLED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.config = {
            ...action.payload,
            stripConfig: action.payload.stripConfig.reduce(function (result, item) {
                result[item.id] = item;
                return result;
            }, {}),
            inputs: {gpioInputs: action.payload.inputs.gpioInputs.reduce(function (result, item) {
                    result[item.pin] = item;
                    return result;
                }, {})},
            supportedEffects: device.backend.getEffectArgumentTypes(device.id).map(effect => effect.replace("Arguments", ""))
        };
    },
    ['CONNECT_TO_DEVICE_PENDING']: (state, action) => {
        const device =state[action.meta.deviceId];
        device.pendingActions++;
        device.failed = false;
    },
    ['GET_DEVICE_CONFIG_PENDING']: (state, action) => {
        state[action.meta.deviceId].pendingActions++;
    },
    ['GET_EFFECT_ARGS_PENDING']: (state, action) => {
        state[action.meta.deviceId].pendingActions++;
    },
    ['GET_EFFECT_ARGS_FULFILLED']: (state, action) => {
        state[action.meta.deviceId].pendingActions--;
        const strip = state[action.meta.deviceId].config.stripConfig[action.meta.stripId];
        const effectArgType = action.payload.effectArguments;
        strip.effectArgs = {...action.payload, effect: effectArgTypeToName(effectArgType)};
    },
    ['SET_EFFECT_ARGUMENTS_PENDING']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions++;
        const strip = device.config.stripConfig[action.meta.stripId];
        const effectArgType = action.meta.newArgs.effectArguments;
        strip.effectArgs = {...action.meta.newArgs, effect: effectArgTypeToName(effectArgType)};
    },
    ['SET_EFFECT_ARGUMENTS_FULFILLED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
    },
    ['SET_EFFECT_ARGUMENTS_REJECTED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = false;
        device.failed = true;
        const strip = device.config.stripConfig[action.meta.stripId];
        strip.effectArgs = action.meta.oldArgs;
    },
    ['SET_CONFIG_PENDING']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions++;
    },
    ['SET_CONFIG_FULFILLED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
    },
    ['SET_CONFIG_REJECTED']: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = false;
        device.failed = true;
    },
    ['SYSTEM_STATUS_UPDATE']: (state, action) =>{
        const device = state[action.meta.deviceId];
        device.systemStatus = action.payload;
    },
    ["DISCONNECT_DEVICE_PENDING"]: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions++;
    },
    ["DISCONNECT_DEVICE_FULFILLED"]: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = false;
        device.failed = false;
    },
    ["DISCONNECT_DEVICE_REJECTED"]: (state, action) => {
        const device = state[action.meta.deviceId];
        device.pendingActions--;
        device.connected = false;
        device.failed = true;
    },
});