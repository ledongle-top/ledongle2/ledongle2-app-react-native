import {BleManager, fullUUID, UUID} from 'react-native-ble-plx';
import base64 from 'react-native-base64';
import * as protobuf from 'protobufjs';
import * as b64 from 'base64-arraybuffer';
import Backend from "./Backend";
import MockBackend from "./MockBackend";
import { Base64 } from 'js-base64';
import {Root} from "protobufjs";
import {PermissionsAndroid} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import merge from "merge-anything";

const SYSTEM_SERVICE_UUID = "5123c0e3-2134-44a9-a2d0-af205586bfdd";
const EFFECT_SERVICE_UUID = "dcbe3082-3cef-481e-8138-362b1c1aa555";


const PROTO_INFO_CHARACTERISTIC_UUID = "35bb00f6-67a9-47ad-8408-2c3c10f77e43";
const SYSTEM_CONFIG_CHARACTERISTIC_UUID = "1a580969-2eaa-44e3-8643-ea0f6c8d5f97";
const SYSTEM_STATUS_CHARACTERISTIC_UUID = "db67de50-3e9a-46bb-9e7e-df3edf9a5eb0";
const SYSTEM_COMMAND_CHARACTERISTIC_UUID = "4e8ca57b-3218-48aa-b1b9-175280e32b6d";


export default class BleBackend extends Backend {

    devices = {};
    constructor() {
        super();
        console.log("Initializing BLE backend");
        this.bleManager = new BleManager();

        this.bleManager.onStateChange((state) => {
            if (state === 'PoweredOn') {
                this.startScan();
            } else {
                console.warn("Bluetooth state changed to %s", state);
            }
        });


        const b64encTable = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".split("");
        const encoder = function (_ary) {
            const len = _ary.length;
            let md = len % 3;
            let b64 = "";
            let i, tmp = 0;

            if (md) for (i = 3 - md; i > 0; i--) _ary[_ary.length] = 0;

            for (i = 0; i < len; i += 3) {
                tmp = (_ary[i] << 16) | (_ary[i + 1] << 8) | _ary[i + 2];
                b64 += b64encTable[(tmp >>> 18) & 0x3f]
                    + b64encTable[(tmp >>> 12) & 0x3f]
                    + b64encTable[(tmp >>> 6) & 0x3f]
                    + b64encTable[tmp & 0x3f];
            }

            if (md) {
                md = 3 - md;
                b64 = b64.substr(0, b64.length - md);
                while (md--) b64 += "=";
            }
            return b64;
        };
        Uint8Array.toBase64 = encoder;
        Uint8Array.prototype.toBase64 = function () {
            return encoder(this);
        };

    }

    readDescriptorChunk(deviceId, read_data, index): Promise{
        return this.bleManager.writeCharacteristicWithResponseForDevice(deviceId, SYSTEM_SERVICE_UUID, PROTO_INFO_CHARACTERISTIC_UUID, Uint8Array.of(index >> 24, index >> 16, index >> 8, index).toBase64()).then((characteristic) => {
            return this.bleManager.readCharacteristicForDevice(deviceId, SYSTEM_SERVICE_UUID, PROTO_INFO_CHARACTERISTIC_UUID).then((characteristic) => {
                if (characteristic.value !== "") {
                    const value = base64.decode(characteristic.value);
                    return this.readDescriptorChunk(deviceId, read_data + value, index + value.length);
                }else {
                    return read_data;
                }
            });
        });
    }

    protoDescriptorKey(deviceId){
        return `protodescriptors-${deviceId}-${this.devices[deviceId].version}`;
    }

    getProtoDescriptors(deviceId){
        console.log("Get descriptors");
        return AsyncStorage.getItem(this.protoDescriptorKey(deviceId)).then((value)=>{
            if(value) {
                return value;
            }else{
                return this.readDescriptorChunk(deviceId, "", 0).then((descriptors) => {
                    return descriptors;
                })
            }
        }).catch( () => {
            return this.readDescriptorChunk(deviceId, "", 0).then((descriptors) => {
                return descriptors;
            })
        }).then((descriptors) =>{
            AsyncStorage.setItem(this.protoDescriptorKey(deviceId), descriptors);
            const protoRoot = new Root();
            descriptors.split("syntax")
                .splice(1)
                .map((str) => "syntax"+str)
                .forEach((str) => protobuf.parse(str, protoRoot));
            return protoRoot;
        });
    }

    connectToDevice(deviceId): Promise {
        return this.bleManager.connectToDevice(deviceId).then((device) => {
            console.info("Connected to: " + deviceId);
            return device.discoverAllServicesAndCharacteristics()

        }).then((device) => {
            console.info("Requesting MTU upgrade from " + device.id);
            return device.requestMTU(500)
        });
    }

    startScan(callback) {

        console.log("Start BLE scan");
        let knownDevices = [];

            this.bleManager.startDeviceScan([SYSTEM_SERVICE_UUID], null, (error, newDevice) => {
                if (error) {
                    if(error.errorCode === 101){
                        try {
                            PermissionsAndroid.request(
                                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                                {
                                    title: 'Location Permission for BLE Scan',
                                    message:
                                        'Android requires location permission to find Bluetooth devices' +
                                        'This app does not track your location',
                                    buttonNeutral: 'Cancel',
                                    buttonPositive: 'OK',
                                },
                            ).then((result) => {
                                if (result === PermissionsAndroid.RESULTS.GRANTED) {
                                    this.startScan(callback);
                                } else {
                                    return;
                                }
                            });
                        } catch (err) {
                            console.warn(err);
                        }
                    }else{
                        console.error(`BLE scan error ${error}`);
                        return;
                    }
                }else{
                console.debug("Saw " + newDevice.id);
                    if (knownDevices.find((existing) => existing === newDevice.id) === undefined) {
                        knownDevices.push(newDevice.id);
                        console.info("Found new device: " + newDevice.id);
                        const fwVersion = newDevice.manufacturerData?base64.decode(newDevice.manufacturerData):"";
                        this.devices[newDevice.id] = merge(this.devices[newDevice.id], newDevice, {version: fwVersion});
                        callback({id: newDevice.id, name: newDevice.name, backend: this, version: fwVersion});
                    }

            }});
    }

    static decodeCharacteristicValue(value, type, protoRoot){
        let byteArray = new Uint8Array(b64.decode(value));
        let decoder = protoRoot.lookupType(type);
        return decoder.toObject(decoder.decode(byteArray), {oneofs: true, defaults: true, enums: String});
    }

    stopScan(callback) {
        this.bleManager.stopDeviceScan();
    }


    getDeviceConfig(deviceId): Promise<Object> {
            return this.getProtoDescriptors(deviceId).then((protoRoot) => {
                this.devices[deviceId].protoRoot = protoRoot;
            }).then(() => {
                return this.bleManager.readCharacteristicForDevice(deviceId, SYSTEM_SERVICE_UUID, SYSTEM_CONFIG_CHARACTERISTIC_UUID)
            }).then((characteristic) => {
                const config = BleBackend.decodeCharacteristicValue(characteristic.value, "SystemConfig", this.devices[deviceId].protoRoot);
                return Object.freeze(config);
            });
    }
    setDeviceConfig(deviceId, config): Promise {
        const systemConfigProto = this.devices[deviceId].protoRoot.lookupType("SystemConfig").encode(config).finish();
        console.log(this.devices[deviceId].protoRoot.lookupType("SystemConfig").decode(systemConfigProto));
        return this.bleManager.writeCharacteristicWithResponseForDevice(deviceId, SYSTEM_SERVICE_UUID, SYSTEM_CONFIG_CHARACTERISTIC_UUID, systemConfigProto.toBase64());
    }

    getEffectArguments(deviceId, stripId): Promise {
        return this.bleManager.readCharacteristicForDevice(deviceId, EFFECT_SERVICE_UUID, stripId).then((characteristic) => {
            return  Object.freeze(BleBackend.decodeCharacteristicValue(characteristic.value, "EffectArguments", this.devices[deviceId].protoRoot));
        });
    }

    setEffectArguments(deviceId, stripId, effectArgs) {
        const effectArgsProto = this.devices[deviceId].protoRoot.lookupType("EffectArguments").encode(effectArgs).finish();
        return this.bleManager.writeCharacteristicWithResponseForDevice(deviceId, EFFECT_SERVICE_UUID, stripId, effectArgsProto.toBase64());
    }
    getEffectArgumentTypes(deviceId) {
        return Object.keys(this.devices[deviceId].protoRoot.nested).filter(typename => (typename.endsWith("Arguments") && typename !== "EffectArguments"));
    }

    getDefaultEffectArgs(deviceId, effect) {
        const effectType = this.devices[deviceId].protoRoot.lookupType(effect);
        let  args = effectType.toObject(effectType.decode(new Uint8Array()), {oneofs: true, defaults: true});
        for(key of Object.keys(args)) {
            if (typeof (args[key] === "number" && !key.includes("olor"))) {
                args[key] = -1;
            }
        }
        return args;
    }
    observeSystemStatus(deviceId, callback){
        this.devices[deviceId].statusSubscription = this.bleManager.monitorCharacteristicForDevice(deviceId, SYSTEM_SERVICE_UUID, SYSTEM_STATUS_CHARACTERISTIC_UUID, (error, characteristic) => {
            if(!error) {
                this.getSystemStatus(deviceId).then((status) => {
                    callback(status);
                })
            }
        });
        this.getSystemStatus(deviceId).then((status) => {
            callback(status);
        })
    }
    getSystemStatus(deviceId) {
        return this.bleManager.readCharacteristicForDevice(deviceId, SYSTEM_SERVICE_UUID, SYSTEM_STATUS_CHARACTERISTIC_UUID).then((characteristic) => {
            return  Object.freeze(BleBackend.decodeCharacteristicValue(characteristic.value, "SystemStatus", this.devices[deviceId].protoRoot));
        }).catch();
    }
    toggleLights(deviceId, state) {
        const commandType = this.devices[deviceId].protoRoot.lookupType("SystemCommand");
        const command = commandType.create({type: state ? 4 : 5});
        return this.bleManager.writeCharacteristicWithResponseForDevice(deviceId, SYSTEM_SERVICE_UUID, SYSTEM_COMMAND_CHARACTERISTIC_UUID, commandType.encode(command).finish().toBase64());
    }

    disconnectDevice(deviceId): Promise {
        return this.bleManager.cancelDeviceConnection(deviceId);
    }
}
