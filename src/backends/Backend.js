export default class Backend{
    constructor(){
        if(this.constructor === Backend){
            throw new Error('Backend is an abstract class and cannot be instantiated directory!');
        }
    }
    startScan(callback){
        throw new Error('Method not implemented');
    }
    stopScan(callback){
        throw new Error('Method not implemented');
    }
    getDeviceConfig(deviceId): Promise<Object>{
        throw new Error('Method not implemented');
    }
    setDeviceConfig(deviceId, config): Promise{
        throw new Error('Method not implemented');
    }
    getDefaultEffectArgs(deviceId, effect){
        throw new Error('Method not implemented');
    }
    connectToDevice(deviceId): Promise{
        throw new Error('Method not implemented');
    }
    disconnectDevice(deviceId): Promise{
        throw new Error('Method not implemented');
    }
    getEffectArguments(deviceId, stripId): Promise{
        throw new Error('Method not implemented');
    }
    setEffectArguments(deviceId, stripId, effectArgs){
        throw new Error('Method not implemented');
    }
    getEffectArgumentTypes(deviceId){
        throw new Error('Method not implemented');
    }
    getEffectArgumentDefaults(deviceId, effect) {
        throw new Error('Method not implemented');
    }
    getSystemStatus(deviceId){
        throw new Error('Method not implemented');
    }
    observeSystemStatus(deviceId, callback) {
        throw new Error('Method not implemented');
    }
    toggleLights(deviceId, state){
        throw new Error('Method not implemented');
    }
}