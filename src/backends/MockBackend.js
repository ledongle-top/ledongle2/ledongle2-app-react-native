import Backend from "./Backend";
import {encodeRGB} from "../util/rgb";

export default class MockBackend extends Backend{
    failedDevices = {};

    static dummyDeviceConfig(){
        return {version: 'dummy', name: 'lorem ipsum', stripConfig:[{id: 0, leds: 20, maxBrightness: 100, favoriteColors:[
                    encodeRGB({r:255, g:0, b:0}),
                    encodeRGB({r:0, g:255, b:0}),
                    encodeRGB({r:0, g:0, b:255}),
                    encodeRGB({r:255, g:255, b:0}),
                    encodeRGB({r:255, g:0, b:255}),
                    encodeRGB({r:0, g:255, b:255}),
                    encodeRGB({r:255, g:255, b:255}),
                ]},
                {id: 1, leds: 1, maxBrightness: 50 }],
            wifi:{ssid: "leeklabs", password: "mikumiku39"},
            updates:{auto_update: true, updateUrl: "https://update.leeklabs.download/"},
            inputs:{gpioInputs:[{pin:1,pullup: true, active_low: false}, {pin: 2, pullup: false, active_low: false}]}
     };
    }


    static dummyEffectArgs(stripId){
        if(stripId == 0){
            return  {effectArguments: "DummyArguments", DummyArguments: {color: 0x0000ffff, reverse: true, speed: 100, text: "Miku"}, brightness: 100};
        }else{
            return {brightness: 50, effectArguments: "LoremArguments", LoremArguments: {lorem: "ipsum"}};
        }
    }

    dummyDevice(){
        return {
            id: Math.random().toString(),
            name: 'Dummy',
            backend: this,
        }
    }
    constructor(){
        super();
        console.log("Starting mock backend device generator loop");
        setInterval(() => {
            if(this.scanning && this.scanCallback){
                console.log("new device");
                this.scanCallback(this.dummyDevice());
            }
        }, 5000);
    }

    startScan(callback){
        this.scanCallback = callback;
        this.scanning = true;
        this.scanCallback({...this.dummyDevice(), id: "393939"});
    }
    stopScan(){
        this.scanning = false;
    }

    getDeviceConfig(device){
        const config = MockBackend.dummyDeviceConfig(device);
        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve(config);
            }, 500);
        });
    }
    setDeviceConfig(deviceId, config): Promise {
        console.log(`set config for ${deviceId}`);
        console.log(config);
        return Promise.resolve();
    }

    connectToDevice(deviceId): Promise {
        if(this.failedDevices[deviceId]) return Promise.reject();
        /*setTimeout(()=> {
            this.failedDevices[deviceId] = true;
        }, 5000);*/
        return new Promise((resolve )=> setTimeout(() => resolve(), 1000) );
    }
    getEffectArguments(device, stripId): Promise {
        if(this.failedDevices[device.id]) return Promise.reject();
        return new Promise((resolve) => {
            setTimeout(() =>resolve(MockBackend.dummyEffectArgs(stripId)), 500);
        })
    }
    setEffectArguments(deviceId, stripId, effectArgs) {
        if(this.failedDevices[deviceId]) return Promise.reject();
        console.log(`set effect args to ${effectArgs}`);
        return Promise.resolve();
    }
    getEffectArgumentTypes(deviceId) {
        return ["DummyArguments", "SolidArguments"];
    }
    getDefaultEffectArgs(deviceId, effect) {
        return {effectArguments: `${effect}`, [`${effect}`]: {color: 255, speed: 0, reverse: false}};
    }
}
