export function encodeRGB({r, g, b}){
    return (r & 0xff) << 16 | (g & 0xff) << 8 | (b & 0xff);
}

export function decodeRGB(rgb){
    return {r:(rgb >> 16) & 0xff, g: (rgb >> 8) & 0xff, b: rgb & 0xff};
}