import React, {Component} from 'react';
import {Text, View, StyleSheet, FlatList, Button, TouchableOpacity, Switch} from 'react-native';
import {Card} from "react-native-elements";
import Slider from '@react-native-community/slider';
import HsvColorPicker from "react-native-hsv-color-picker";
import * as colorConvert from "color-convert";
import {decodeRGB, encodeRGB} from "../util/rgb";
import tinycolor from "tinycolor2";
import {HueSlider, LightnessSlider, SaturationSlider, SlidersColorPicker, ValueSlider} from "react-native-color";
import Spinner from 'react-native-loading-spinner-overlay';
import Dialog from "react-native-dialog";
import {Colors} from "./AppStyle";


class StripTab extends Component {
    constructor(props) {
        super(props);
        console.log("hi");
        this.state = {
            rateLimitingActive: false,
            pendingUpdate: null
        }
    }

    updateEffectArgs(key, value){
        const args = this.props.strip.effectArgs[this.props.strip.effectArgs.effectArguments];
        console.log(`Set ${key} to ${value}`);
        const newArgs = {...args, ...this.state.pendingUpdate, [key]: value};
        if(!this.state.rateLimitingActive) {
            this.state.rateLimitingActive = true;
            setTimeout(() => {
                if(this.state.pendingUpdate) {
                    this.props.onArgsUpdate({...this.props.strip.effectArgs, [this.props.strip.effectArgs.effectArguments]: newArgs});
                }
                this.setState({rateLimitingActive: false, pendingUpdate: null});
            }, 200);
            this.props.onArgsUpdate({...this.props.strip.effectArgs, [this.props.strip.effectArgs.effectArguments]: newArgs});
        }else {
            this.setState({pendingUpdate: newArgs});
        }

    }

    uiFromEffectArgs(args, onArgsUpdate) {
        let ui = [];
        const keys = Object.keys(args);
        keys.sort((a, b) => {
            const aIsColor = a.toLowerCase().includes("color");
            const bIsColor = b.toLowerCase().includes("color");
            if(aIsColor && !bIsColor){
                return -1;
            }else if(bIsColor && !aIsColor){
                return 1;
            }else{
                return a < b?-1:1;
            }
        });
        for (const key of keys) {
            const value = args[key];
            if (key === "color") {
                ui.push(<ColorPickerCard key={key} title={key} args={args} argKey={key} setter={onArgsUpdate} favoriteColors={this.props.strip.favoriteColors}/>);
            } else {
                switch (typeof (value)) {
                    case "number": {
                        ui.push(<NumberSliderCard title={key} args={args} key={key} argKey={key} setter={onArgsUpdate}/>);
                        break;
                    }
                    case "boolean":{
                        ui.push(<ToggleCard title={key} args={args} key={key} argKey={key} setter={onArgsUpdate}/>);
                        break;
                    }
                    default: {
                        ui.push(<Card dividerStyle={styles.cardDividerStyle} setter={onArgsUpdate} args={args}
                                      key={key} title={key} titleStyle={styles.cardTitleStyle}
                                      containerStyle={styles.cardContainerStyle}><Text
                            key={value}>{"Name: " + key + " Type: " + typeof (value) + " val: " + value.toString()}</Text>
                        </Card>);
                        break;
                    }
                }
            }
        }
        return ui;
    }


    render = () => {

        const ui = this.uiFromEffectArgs(this.props.strip.effectArgs[this.props.strip.effectArgs.effectArguments], this.updateEffectArgs.bind(this));
        return (
            <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Spinner visible={!this.props.device.connected}/>
                {ui}</View>
        )
    };
}

class ColorPickerCard extends Component {
    constructor(props) {
        super(props);
        const color = tinycolor(decodeRGB(props.args[props.argKey])).toHsv();
        this.state = {
            color: {...color, s: 1, v: 1},

        };
    }
    onUpdateColor(colorHSV){
        this.setState({color: colorHSV});
            const color = encodeRGB(tinycolor(colorHSV).toRgb());
            if(color === 0){
                console.warn("WTF");
            }
            this.props.setter(this.props.argKey,color);
        }

    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {


        return<Card  titleNumberOfLines={1} containerStyle={[styles.cardContainerStyle, {flex: 0, height: 220}]} titleStyle={styles.cardTitleStyle}>
            <HueSlider
                style = {{top: 0}}
                gradientSteps={40}
                value={this.state.color.h}
                onValueChange={hue => {
                    console.log(hue);

                    this.onUpdateColor({...this.state.color, h: hue});
                }}
                onSlidingComplete={ () => {
                    this.onUpdateColor(this.state.color)
                }}
            />
            <SaturationSlider color={this.state.color}
                         gradientSteps={40}
                         onValueChange={(saturation) =>{
                             this.onUpdateColor({...this.state.color, s: 1-saturation});
                         }}
                              thumbStyle={{height: 32, width: 24}}
                              containerStyle={{height: 48, marginTop: 0}}
                         value={1-this.state.color.s}>
            </SaturationSlider>
            <View style={{flexDirection: 'row', alignItems: "space-between" }}>
            {this.props.favoriteColors.map((color) => {return <ColorTile
                color={color}
                onPress={()=>{
                    this.onUpdateColor(tinycolor(decodeRGB(color)).toHsv());
                }}
                style={{maxWidth:48, flex: 1, height: 48, borderRadius: 10, borderColor: "black", borderStyle: "solid", borderWidth: 1, marginLeft: 5, marginRight: 5, marginBottom: 5}}
                key={Math.random().toString()}
            />})}
            </View>
            <CardLabel text={this.props.title}/>
        </Card>;
    }
}

class ColorTile extends Component{
    render(){
        return <TouchableOpacity style={[{
            backgroundColor: tinycolor(decodeRGB(this.props.color)).toHexString()},
            this.props.style]}
                                 onPress = {this.props.onPress}

        />
    }
}

class ToggleCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            checked: this.props.args[this.props.argKey],
        }
    }
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <Card title={this.props.title} titleStyle={styles.cardTitleStyle}
                            containerStyle={[styles.cardContainerStyle, {flex: 0, height: 50}]}
                            dividerStyle={styles.cardDividerStyle}>
            <Switch style={{alignSelf: 'center'}} value={this.state.checked} onValueChange={newVal => {
                this.setState({newVal});
                this.props.setter(this.props.argKey, value);
            }}/>
            <CardLabel text={this.props.title}/>
        </Card>;
    }
}

class NumberSliderCard extends Component {
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <Card title={this.props.title} titleStyle={styles.cardTitleStyle}
                     containerStyle={[styles.cardContainerStyle, {flex: 0, height: 60}]}
                     dividerStyle={styles.cardDividerStyle}>
            <Slider style={{top: 10, margin: 10}} maximumValue = {255} onValueChange={(value) => this.props.setter(this.props.argKey, value)}/>
            <CardLabel text={this.props.title}/>
        </Card>
    }
}

class CardLabel extends Component{
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <Text style={styles.cardLabel}>{this.props.text}</Text>;
    }
}

const styles = StyleSheet.create({
    cardContainerStyle: {
        flex: 1,
        padding: 0,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 1,
        marginRight: 1,
        width: "100%",
        borderRadius: 20,
        overflow: 'hidden',
        borderWidth: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignContent: 'flex-start'
    },
    cardTitleStyle: {
        marginBottom: 0,
        height: 0,
        margin: 0,
        padding:0
    },
    cardDividerStyle: {
        marginBottom: 0
    },
    cardLabel: {
        alignSelf: 'flex-end',
        right: 10,
        top: 1,
        fontWeight: 'bold',
        color: Colors.main
    }
});


export default StripTab;
