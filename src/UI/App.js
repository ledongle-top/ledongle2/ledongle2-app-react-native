/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @flow
 */

import React, {Component} from 'react';
import {View,Dimensions, SafeAreaView,Text, ActivityIndicator} from 'react-native';
import {TabBar, TabView} from 'react-native-tab-view';
import MockBackend from '../backends/MockBackend.js';
import {connect} from 'react-redux';
import ScanTab from './ScanTab.js';
import StripTab from './StripTab.js';
import {DeviceToolbar, ScanToolbar} from "./BottomToolbar";
import {AppStyle, Colors} from "./AppStyle";
import {
    addConnector, changeEffect,
    deviceStateUpdate, getCompleteDeviceConfig, getDeviceConfig,
    getEffectArguments, setDeviceConfig, setEffectArgs,
    startScan,
    stopScan, toggleLights
} from "../DeviceManagement/DeviceManager";
import BleBackend from "../backends/BleBackend";
import DeviceInfo from 'react-native-device-info'
import Toast from 'react-native-root-toast';
import {store} from "../DeviceManagement/Store";

type Props = {};

class App extends Component<Props> {

    static defaultProps = {strips: []};

    constructor() {
        super();
        this.state = {
            index: 0,
            // toolbarButtons: ScanTab.toolbarButtons
        };
        store.subscribe((arg) => {console.log(`STATE CHANGE ${arg}`)});
    }


    componentDidMount() {
        if (__DEV__) {
            this.props.addConnector(new MockBackend());
        }
        if(!DeviceInfo.isEmulator()) {
            this.props.addConnector(new BleBackend());
        }
        this.props.startScan();
    }

    render() {
        return (
            <SafeAreaView style={AppStyle.container}>
                <TabView
                    navigationState={{
                        index: this.state.index, routes: [
                            {key: "scan", title: this.props.strips.length >= 1 ? "SCAN" : "SCAN FOR DEVICES"},
                            ...this.props.strips.filter(({device, stripId})=> {
                                return !device.failed
                            }).map(
                                    ({device, stripId}) => {
                                        return {key: `${device.id}${stripId}`, title: device.name, device: device, stripId: stripId}
                                    }
                                )
                        ]
                    }}
                    renderTabBar={props =>
                        <TabBar
                            {...props}
                            indicatorStyle={{backgroundColor: 'white'}}
                            scrollEnabled={this.props.strips.length > 2}
                            renderIndicator={this.props.strips.length === 0 ? () => {
                            } : undefined}
                            style={{backgroundColor: Colors.main}}
                            activeColor={Colors.accent}
                            renderLabel={ ({route}) => <View style={{flexDirection:'row', alignItems: 'center'}}>
                                <ActivityIndicator
                                animating={route.device !== undefined && (route.device.pendingActions > 1)}
                                />
                            <Text style={{color: "white"}}>{route.title}</Text>
                                <ActivityIndicator
                                    animating={false}
                                />
                            </View>}

                        />

                    }
                    renderScene={({route}) => {
                        switch (route.key) {
                            case 'scan':
                                return <ScanTab deviceList = {Object.values(this.props.devices)} startScan = {this.props.startScan} stopScan = {this.props.stopScan}
                                connectToDevice = {((device) => {
                                    this.props.getDeviceAndEffectConfig(device.id) })}/>;
                            default:
                                return <StripTab device = {route.device} strip={route.device.config.stripConfig[route.stripId]}
                                                 onArgsUpdate={(effectArgs) => {
                                                     console.log("updating args");
                                                     this.props.setEffectArguments(route.device.id, route.stripId, effectArgs)
                                                 }}/>;
                        }
                    }
                    }
                    onIndexChange={index => {
                        if(index === 0){
                            this.state.inactivityScanTimeout && clearTimeout(this.state.inactivityScanTimeout);
                        }else{
                            this.setState({inactivityScanTimeout: setTimeout(() => {this.props.stopScan()}, 5000)});
                        }
                        this.setState({index: index })
                    }}
                    initialLayout={{width: Dimensions.get('window').width}}/>
                {this.state.index === 0 ? <ScanToolbar isScanning={this.props.backends.scanning}
                                                       startScan={this.props.startScan} stopScan={this.props.stopScan}
                    />
                    :this.props.strips[this.state.index - 1]&& <DeviceToolbar
                        device = {this.props.strips[this.state.index - 1].device}
                        setConfig={this.props.setConfig}
                        stripId = {this.props.strips[this.state.index - 1].stripId}
                        changeEffect = {this.props.changeEffect}
                        toggleLights = {this.props.toggleLights}
                    />}
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        strips: Object.values(state.devices)
            .flatMap((device) =>
                device.config && Object.values(device.config.stripConfig).map(strip =>
                {return strip.effectArgs?
                    {device: device, stripId: strip.id}:
                    undefined
                }))
            .filter(Boolean)
            .filter((strip) => {
                return Boolean(strip.device.config.stripConfig[strip.stripId].effectArgs);
            }),
        backends: state.backends,
        devices: state.devices
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addConnector: connector => {
            return dispatch(addConnector(connector))
        },
        startScan: () => {
            return dispatch(startScan())
        },
        stopScan: () => {
            return dispatch(stopScan())
        },
        deviceStateUpdate: device => {
            return dispatch(deviceStateUpdate(device));
        },
        getEffectArguments: (device, stripId) => {
          return dispatch(getEffectArguments(device, stripId));
        },
        getDeviceConfig: (device) => {
            return dispatch(getDeviceConfig(device));
        },
        getDeviceAndEffectConfig: (deviceId) => {
            return dispatch(getCompleteDeviceConfig(deviceId));
        },
        setEffectArguments: (deviceId, stripId, effectArgs) => {
            return dispatch(setEffectArgs(deviceId, stripId, effectArgs));
        },
        setConfig: (deviceId, config) => {
            return dispatch(setDeviceConfig(deviceId,config));
        },
        changeEffect: (deviceId, stripId, effect) => {
            return dispatch(changeEffect(deviceId, stripId, effect));
        },
        toggleLights: (deviceId, state) => {
            return dispatch(toggleLights(deviceId, state));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
