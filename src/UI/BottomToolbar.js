import React from "react";
import {SafeAreaView, TouchableOpacity, View, StyleSheet, ScrollView, FlatList} from "react-native";
import {AppStyle, Colors} from "./AppStyle";
import {Button, Card, Icon, Text} from "react-native-elements";
import Dialog from "react-native-dialog";
import ObjectEditor from "./ObjectEditor";
import merge from "merge-anything";

export class ScanToolbar extends React.Component {
    componentDidMount(): void {
        console.log(this.props)
    }

    render() {
        console.log(this.props);
        return <BottomToolbar icons={[{
            name: 'bluetooth', onPress: () => {
                console.log("soos")
            }
            , color:Colors.panel
        },
            {name: 'cancel', color:Colors.panel},
            {
                type: 'material-community',
                name: 'radar',
                color: this.props.isScanning ? Colors.accent : Colors.inactive,
                onPress: this._onPress
            },
            {name: 'cloud', color:Colors.panel},
            {name: 'settings', color:Colors.panel}]}/>
    }

    _onPress = () => {
        console.log(this.props);
        if (this.props.isScanning === true) {
            this.props.stopScan();
        } else {
            this.props.startScan();
        }
    }

}

export class DeviceToolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            settingsVisible: false,
            advancedSettingsVisible: false,
            effectListVisible: false,
            wifiSettingsVisible: false,
            lightState: true,
        };
    }

    wifiIconName(device) {
        if (device && device.systemStatus) {
            if (device.systemStatus.wifiState === "CONNECTED") {
                if (device.systemStatus.updateStatus === "UPDATE_AVAILABLE") {
                    return "wifi-star";
                } else {
                    return "wifi";
                }
            } else {
                return "wifi-off";
            }
        } else {
            return "wifi-off";
        }
    }

    wifiIconColor(device) {
        if (device && device.systemStatus) {
            if (device.systemStatus.wifiState === "CONNECTED") {
                if (device.systemStatus.updateStatus === "UPDATE_AVAILABLE") {
                    return Colors.accent;
                } else {
                    return Colors.active;
                }
            } else {
                return Colors.inactive;
            }
        } else {
            return Colors.inactive;
        }
    }

    render() {
        return <View><BottomToolbar icons={[
            {
                name: 'lightbulb', onPress: () => {
                    this.props.toggleLights(this.props.device.id, this.state.lightState);
                    this.setState({lightState: !this.state.lightState});
                },
                color: Colors.active,
                type: "material-community",
            },
            {name: 'cancel', color:Colors.main},
            {
                name: 'flare', onPress: () => {
                    this.setState({effectListVisible: true})
                },
                color: Colors.active
            },
            {name: this.wifiIconName(this.props.device), type: "material-community",
            color: this.wifiIconColor(this.props.device),
                onPress:() => this.setState({wifiSettingsVisible: true})
            },
            {
                name: 'settings', color: Colors.active, onPress: () => {
                    this.setState({settingsVisible: true})
                }
            }]}/>
            <WifiDialog device={this.props.device} visible={this.state.wifiSettingsVisible} dismiss={() => {this.setState({wifiSettingsVisible: false})}} showSettings={() => this.setState({settingsVisible: true})}/>
            <EffectSelectionDialog visible={this.state.effectListVisible} device={this.props.device} changeEffect={this.props.changeEffect} stripId={this.props.stripId} dismiss={() => {this.setState({effectListVisible: false})}}/>
            <DeviceSettingsDialog visible={this.state.settingsVisible} device={this.props.device} setConfig={this.props.setConfig} stripId={this.props.stripId}  dismiss={() => {this.setState({settingsVisible: false, advancedSettingsVisible: false})}}/>
        </View>
    }
}
class WifiDialog extends React.Component{

    getWifiStatusText(wifiStatus){
        if(!wifiStatus) return "Unknown";
        switch(wifiStatus){
            case "CONNECTED":
                return "Connected";
            case "CONNECTING":
                return "Connecting";
            default:
                return "Disconnected";
        }
    }
    shouldComponentUpdate(newProps, newState){return true;}

    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <Dialog.Container visible={this.props.visible}>
            <Dialog.Title>Device Updates</Dialog.Title>
            <Text>Wifi Status: {this.getWifiStatusText(this.props.device.status && this.props.device.status.wifiState)}</Text>
            <Dialog.Button label="Settings" onPress={() =>{
                this.props.dismiss();
                this.props.showSettings();
            }}/>
            <Dialog.Button label="Close" onPress={this.props.dismiss}/>
        </Dialog.Container>
    }
}


class EffectSelectionDialog extends React.Component{
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <Dialog.Container visible={this.props.visible}>
            <View style={{flex: 1}}>
                <FlatList data={this.props.device.config.supportedEffects}
                          renderItem={({item}) => {
                              return <EffectCard effect={item}
                                                 isSelected={item === this.props.device.config.stripConfig[this.props.stripId].effectArgs.effect}
                                                 onEffectChange={() => {
                                                     this.props.dismiss();
                                                     this.props.changeEffect(this.props.device.id, this.props.stripId, item)
                                                 }
                                                 }
                                                 dismissDialog={this.props.dismiss}
                              />
                          }}
                          keyExtractor={(item) => item}
                />
            </View>
            <Button title="Cancel" onPress={this.props.dismiss}/>
        </Dialog.Container>
    }
}

class DeviceSettingsDialog extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            advancedSettingsVisible: false
        }
    }
    shouldComponentUpdate(newProps, newState){return true;}
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const config = this.props.device.config;
        let settingsChanges = {};
        return (<View>
            <Dialog.Container visible={this.props.visible}>
                <Dialog.Title>{this.state.advancedSettingsVisible ? "Advanced Settings" : "Device Settings"}</Dialog.Title>

                <ScrollView style={{flex: 1}}>
                    {this.state.advancedSettingsVisible ?
                        <ObjectEditor key={"shutupreact"}
                                      objectToEdit={this.props.device.config}
                                      objectKey="systemConfig"
                                      changeContainer={settingsChanges}
                        /> :
                        <View>

                            <ObjectEditor key={"bla"}
                                          objectToEdit={{
                                              name: config.name,
                                              stripConfig: {[this.props.stripId]: {leds: config.stripConfig[this.props.stripId].leds}},
                                              wifi: {ssid: config.wifi.ssid, password: config.wifi.password},
                                              updates: {auto_update: config.updates.auto_update}
                                          }}
                                          objectKey={"systemConfig"}
                                          changeContainer={settingsChanges}
                            />
                        </View>}
                </ScrollView>
                {this.state.advancedSettingsVisible ? <Dialog.Description>
                        BE VERY CAREFUL! Wrong settings can cause the device to malfunction including completely bricking
                        it!
                    </Dialog.Description>
                    :
                    <Button onPress={() => this.setState({advancedSettingsVisible: true})} title={"Advanced"}/>}
                <Dialog.Button label="Cancel"
                               onPress={this.props.dismiss}/>
                <Dialog.Button label="Save" onPress={() => {
                    this.props.dismiss();
                    console.log(settingsChanges);
                    let newConfig = merge({}, this.props.device.config, settingsChanges);
                    this.props.setConfig(this.props.device.id, newConfig);
                }
                }/>
            </Dialog.Container>
        </View>)
    }
}


class EffectCard extends React.Component {
    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return <TouchableOpacity onPress={this.props.isSelected ? this.props.dismissDialog : this.props.onEffectChange}>
            <Card containerStyle={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: this.props.isSelected ? Colors.accent : undefined
            }}>
                <Text style={{alignSelf: 'center',}}>
                    {this.props.effect}
                </Text>
            </Card>
        </TouchableOpacity>
    }
}

export class BottomToolbar extends React.Component {
    render() {
        return (
            <View>
                <SafeAreaView style={styles.blocker} forceInset={{
                    top: 'never',
                    bottom: 'always',
                }}/>
                <SafeAreaView style={styles.toolbarWrapper} pointerEvents="box-none"
                              forceInset={{
                                  top: 'never',
                                  bottom: 'always',
                              }}>
                    <View style={[styles.toolbarContent]} pointerEvents="box-none" forceInset={{
                        top: 'never',
                        bottom: 'always',
                    }}>
                        <TouchableOpacity style={styles.toolbarButton} title="1" onPress={this.props.icons[0].onPress}>
                            <Icon {...this.props.icons[0]} size={toolbarIconSize} onPress={undefined}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.toolbarButton} title="2" onPress={this.props.icons[1].onPress}>
                            <Icon {...this.props.icons[1]} size={toolbarIconSize} onPress={undefined}/>
                        </TouchableOpacity>
                        <View style={styles.mainIconContainer} pointerEvents="box-none">
                            <TouchableOpacity pointerEvents="box-none" style={styles.toolbarMainButton} title="3"
                                              onPress={this.props.icons[2].onPress}>
                                <Icon {...this.props.icons[2]} size={toolbarMainIconSize} onPress={undefined}/>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={styles.toolbarButton} title="4" onPress={this.props.icons[3].onPress}>
                            <Icon {...this.props.icons[3]} size={toolbarIconSize} onPress={undefined}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.toolbarButton} title="5" onPress={this.props.icons[4].onPress}>
                            <Icon {...this.props.icons[4]} size={toolbarIconSize} onPress={undefined}/>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

const toolbarIconSize = 39;
const toolbarMainIconSize = 80;


const styles = StyleSheet.create({
    blocker: {
        width: "100%",
        height: 60,
        backgroundColor: Colors.panel
    },
    toolbarMainButtonContainer: {
        height: 100,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        zIndex: 1,
        borderRadius: 50
    },
    toolbarContent: {
        height: 100,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: 'flex-end'
    },
    toolbarButton: {
        alignContent: 'center',
        width: 50,
        height: 50,
        //   backgroundColor: '#00ff00',
        alignSelf: 'flex-end',
        borderRadius: 50,
        bottom: 5,
        justifyContent: 'center',
    },
    toolbarWrapper: {
        flex: 1,
        position: "absolute",
        height: 100,
        bottom: 0,
        width: "100%"
    },
    toolbarMainButton: {
        justifyContent: 'center',
        alignContent: 'center',
        width: 100,
        height: 100,
        backgroundColor: Colors.main,
        borderRadius: 50
    },
});