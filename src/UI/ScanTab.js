import React, {Component} from 'react';
import { ActivityIndicator, Text, View, FlatList, Button, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {Card} from "react-native-elements";
import {Icon} from "react-native-elements";

import {requestDeviceState, startScan, scanStopped, getDeviceConfig} from "../DeviceManagement/DeviceManager";
import {Colors} from "./AppStyle";

class ScanTab extends Component{

    render = () => {
        return(
                <View style={{alignItems: 'stretch',flex: 1,
                                                     flexDirection: 'column',
                                                     justifyContent: 'space-between'}}>

                <FlatList
            contentContainerStyle={{ flexGrow: 1 }}
                                   data={this.props.deviceList}
                                   keyExtractor={(device) => device.id.toString()}
                                         renderItem={({item}) =>{ return <DeviceListItem
                                                                               item={item}
                                                                               onPress={()=>this.props.connectToDevice(item)}
                                                                               />
                                                            }
                                              }
                                      />

                                       </View>)
                      };
}

class DeviceListItem extends Component{
    render = () => {
        return <TouchableOpacity onPress={this.props.onPress}>
            <Card containerStyle={{borderRadius: 20}}>
                <View style={{justifyContent: "space-between", flexDirection: "row", flex:1, width:"100%", alignItems: this.props.item.pendingActions !== 0?"flex-start":undefined}}>
                    <Text>{this.props.item.name}</Text>
                    {__DEV__ &&
                    <Text>{this.props.item.version}</Text>
                    }
                    <ActivityIndicator animating= {this.props.item.pendingActions !== 0}  color={Colors.accent} size="small"/>
                    {
                    }
                </View>
            </Card>
        </TouchableOpacity>
    }
}


export default ScanTab;
