import {StyleSheet} from "react-native";

export const AppStyle = StyleSheet.create({

    container: {
        flex: 1,

        backgroundColor: '#454C4F',
    },

    tabBar: {
        flexDirection: 'row',
        backgroundColor: "#102028"
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        backgroundColor: "#102028"
    },



});
export const Colors = {
    main: "#373f4c",
    accent: "#00fffc",
    active: "#a4a4a4",
    inactive: "#151716",
    panel: "#102028",
    rainbowGradient: ["#ff0000", "#00ff00","#0000ff"]
};