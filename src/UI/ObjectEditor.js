import React, {Component} from 'react';
import {
    Cell,
    CellGroup,
    CellInput,
    TagsInput,
    SelectList,
    CellSheet,
    ActionItem,
    CellDatePicker,
    CellListProvider,
    CellListItem,
    CellSlider,
    CellSwitch
} from 'react-native-cell-components';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    StatusBar
} from 'react-native';




class ObjectEditor extends Component{
    constructor(props){
        super(props);
        this.state = {changes: this.props.changeContainer};
    }
    generateEditorUi(objectToEdit, objectKey, changeContainer){
        let items = [];
        for(const key of (Object.keys(objectToEdit))){
            const value = objectToEdit[key];
            switch(typeof(value)){
                case "number":
                    items.push(<CellInput  keyboardType={"number-pad"} onChangeText={(text) =>{ console.log("edith"); changeContainer[key] = parseInt(text); this.setState({changes: changeContainer})}} key={key}  title={key} value={
                        this.state.changes[key]!==undefined?
                            isNaN(this.state.changes[key])?"":
                            this.state.changes[key].toString():
                            value.toString()
                    }/>);
                    break;
                case "boolean":
                    items.push(<CellSwitch title={key} key={key} onValueChange={(value) => {changeContainer[key] = value; this.setState({changes: changeContainer})}} value={(this.state.changes[key]!==undefined)?changeContainer[key]:objectToEdit[key]}/>);
                    break;
                case "object":

                    changeContainer[key] = Array.isArray(value)?[]:{};
                    items.push(<ObjectEditor key={key} objectToEdit={value} objectKey={key} changeContainer={changeContainer[key]}/>);

                    break;
                default:
                    items.push(<CellInput onChangeText={(text) =>{ console.log("edith"); changeContainer[key] = text; this.setState({changes: changeContainer})}} key={key}  title={key} value={this.state.changes[key]?this.state.changes[key]:value?value.toString():""}/>);
                    break;
            }
        }
        return items;
    }

    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
       return <CellGroup {...this.props} header={this.props.objectKey} style={styles.container} key={this.props.objectKey}>{this.generateEditorUi(this.props.objectToEdit, this.props.objectKey, this.props.changeContainer)}</CellGroup>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f3f3f3'
    },
    header: {
        height: 40,
        backgroundColor: '#1abc9c'
    },
    tagsInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    tagSelected: {

    },
    tag: {

    }
});

export default ObjectEditor