/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/UI/App';
import React from 'react';
import {name as appName} from './app.json';
import {store} from './src/DeviceManagement/Store.js';
import {Provider} from 'react-redux';

const RNRedux = () => (
        <Provider store = { store }>
        <App />
        </Provider>
)


AppRegistry.registerComponent(appName, () => RNRedux);
